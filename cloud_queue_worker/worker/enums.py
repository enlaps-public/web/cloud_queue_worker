from enum import Enum


class WorkerResult(Enum):
    ACK = 1
    UNACK = 2
