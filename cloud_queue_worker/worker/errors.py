class QueueArgumentError(Exception):
    pass


class WorkerResultUnknown(Exception):
    pass
